@extends('layouts.app')

@section('title')
    Editar Usuario
@endsection


@section('content')
    <div class="h-100 d-flex flex-column">
        <div class="container-fluid flex-1-0-auto bg-gradian-blue-lit linea-blue position-relative d-flex">
            <div
                class="container w-1450 d-flex justify-content-center align-items-center position-relative z-index-99 flex-column pb-100">
                <h1 class="fz-40 fz-md-60 white fw-bold text-center mb-4 mt-50 Jellee-Roman mb-3">Usuario</h1>
                <div class="row w-100 d-flex justify-content-center">
                    <div class="col-md-8 mb-3 mb-md-0 px-4">
                        <div class="position-relative w-910">
                            <a href="/usuario" class="text-decoration-none"><i
                                    class="fas fa-caret-left fz-60 white position-absolute icon-atras"></i></a>
                        </div>
                        <div
                            class="bg-white br-radius-16 shadow--1 px-0 py-5 d-flex justify-content-center align-items-center flex-column position-relative linea-2 overflow-hidden">
                            <div class="w-100 position-relative d-flex justify-content-center">
                                <div class="circle-aff">
                                    <div
                                        class="circle d-flex justify-content-center align-items-center shadow--1 position-relative">
                                        <i class="fas fa-stethoscope blue fz-80"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100 shadow--1 py-3 px-3 mt-50">
                                <h1 class="fz-35 proxima-Nova-Bold dark-1 mb-0 text-center">Editar Usuario</h1>
                            </div>
                            <div class="w-100 px-3 pt-4">
                                @if (isset($usuario) && !empty($usuario))
                                    <form action="{{ $usuario->id }}" method="post" class="mb-0">
                                        @csrf
                                        @method('PUT')
                                        <div class="form-floating mb-3">
                                            <input type="text" maxlength="50"
                                                class="form-control br-radius-16 px-3 pt-4 pb-2" id="nombreCompletoUsuEdit" name="nombreCompletoUsuEdit"
                                                placeholder="Nombre Completa" value="{{$usuario->nombre}}" required>
                                            <label for="nombreCompletoUsuEdit" class="dark-1">Nombre completo</label>
                                        </div>
                                        <div class="form-floating mb-3">
                                            <input type="text" maxlength="10"
                                                class="form-control br-radius-16 px-3 pt-4 pb-2" id="cedulaUsuEdit" name="cedulaUsuEdit"
                                                placeholder="Cedula" value="{{$usuario->cedula}}" required>
                                            <label for="cedulaUsuEdit" class="dark-1">Cedula Ciudadania</label>
                                        </div>
                                        <div class="form-floating mb-3">
                                            <input type="text" maxlength="50"
                                                class="form-control br-radius-16 px-3 pt-4 pb-2" id="direccionUsuEdit" name="direccionUsuEdit"
                                                placeholder="Dirección" value="{{$usuario->direccion}}" required>
                                            <label for="direccionUsuEdit" class="dark-1">Dirección</label>
                                        </div>
                                        <div class="form-floating mb-3">
                                            <input type="text" maxlength="10"
                                                class="form-control br-radius-16 px-3 pt-4 pb-2"
                                                id="telefonoContactoUsuEdit" name="telefonoContactoUsuEdit" placeholder="Telefono contacto"
                                                value="{{$usuario->telefono}}" required>
                                            <label for="telefonoContactoUsuEdit" class="dark-1">Telefono</label>
                                        </div>
                                        <div class="form-floating mb-3">
                                            <input type="text" maxlength="50"
                                                class="form-control br-radius-16 px-3 pt-4 pb-2" id="personaContactoUsuEdit" name="personaContactoUsuEdit"
                                                placeholder="Persona en contacto" value="{{$usuario->persona_contacto}}" required>
                                            <label for="personaContactoUsuEdit" class="dark-1">Persona contacto</label>
                                        </div>
                                        <div class="d-flex justify-content-center">
                                            <button type="submit"
                                                class="fz-20 fw-bold bg-blue white br-none br-radius-16 py-2 px-2 w-300 btn-disabled"
                                                id="btn-editarUsuario">Editar</button>
                                        </div>
                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script src="{{ asset('js/editar_usuario.js') }}" defer></script>
