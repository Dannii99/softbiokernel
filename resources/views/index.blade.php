
@extends('layouts.app')

@section('title')
    Daskboard
@endsection


@section('content')
  <div class="h-100 d-flex flex-column">
    <div class="container-fluid flex-1-0-auto bg-gradian-blue-lit linea-blue position-relative d-flex">
      <div class="container w-1450 d-flex justify-content-center align-items-center position-relative z-index-99 flex-column flex-lg-row py-5">
        <h1 class="white fw-bold fz-40 fz-md-60 Jellee-Roman">Dashboard</h1>
        <div class="row w-100 d-flex justify-content-center">

          <div class="col-10 col-lg-6 col-xl-4 px-4 mb-3">
            <a href="/admin" class="text-decoration-none">
              <div class="bg-white br-radius-16 shadow--1 px-0 py-5 d-flex justify-content-center align-items-center flex-column position-relative linea-2 overflow-hidden">
                <div class="w-100 position-relative d-flex justify-content-center">
                  <div class="circle-aff">
                    <div class="circle d-flex justify-content-center align-items-center shadow--1 position-relative">
                      <i class="fas fa-user-cog blue fz-80"></i>
                    </div>
                  </div>
                </div>
                <h1 class="fz-48 proxima-Nova-Bold fw-bold dark-1 mb-3 text-center mt-5">Admin</h1>
              </div>
            </a>
          </div>

          <div class="col-10 col-lg-6 col-xl-4 px-4 mb-3">
            <a href="/doctor" class="text-decoration-none">
              <div class="bg-white br-radius-16 shadow--1 px-0 py-5 d-flex justify-content-center align-items-center flex-column position-relative linea-2 overflow-hidden">
                <div class="w-100 position-relative d-flex justify-content-center">
                  <div class="circle-aff">
                    <div class="circle d-flex justify-content-center align-items-center shadow--1 position-relative">
                      <i class="fas fa-user-md  blue fz-80"></i>
                    </div>
                  </div>
                </div>
                <h1 class="fz-48 proxima-Nova-Bold fw-bold dark-1 mb-3 text-center mt-5">Doctor</h1>
              </div>
            </a>
          </div>

        </div>
      </div>
    </div>

  </div>
  @endsection