@extends('layouts.app')

@section('title')
    Crear Doctor
@endsection


@section('content')
    <div class="h-100 d-flex flex-column">
        <div class="container-fluid flex-1-0-auto bg-gradian-blue-lit linea-blue position-relative d-flex">
            <div
                class="container w-1450 d-flex justify-content-center align-items-center position-relative z-index-99 flex-column pb-100">
                <h1 class="fz-40 fz-md-60 white fw-bold text-center mb-4 mt-50 Jellee-Roman mb-3">Doctor</h1>
                <div class="row w-100 d-flex justify-content-center">
                    <div class="col-md-8 mb-3 mb-md-0 px-4">
                        <div class="position-relative w-910">
                            <a href="./admin" class="text-decoration-none"><i
                                    class="fas fa-caret-left fz-60 white position-absolute icon-atras"></i></a>
                        </div>
                        <div
                            class="bg-white br-radius-16 shadow--1 px-0 py-5 d-flex justify-content-center align-items-center flex-column position-relative linea-2 overflow-hidden">
                            <div class="w-100 position-relative d-flex justify-content-center">
                                <div class="circle-aff">
                                    <div
                                        class="circle d-flex justify-content-center align-items-center shadow--1 position-relative">
                                        <i class="fas fa-stethoscope blue fz-80"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100 shadow--1 py-3 px-3 mt-50">
                                <h1 class="fz-35 proxima-Nova-Bold dark-1 mb-0 text-center">Crear Doctor</h1>
                            </div>
                            <div class="w-100 px-3">
                                @if ($crear)
                                <button type="button"
                                    class="fz-18 fw-bold br-none bg-blue white br-radius-16 px-5 py-2 mt-3 shadow--1 cursor-pointer"
                                    data-bs-toggle="modal" data-bs-target="#crear-doctor">Crear <i
                                        class="fas fa-plus ms-2"></i>
                                </button>
                                @endif
                                
                                @if (isset($doctor) && !empty($doctor))
                                    <ul class="list-group list-group-flush px-0 mt-4">
                                        @foreach ($doctor as $count => $i )
                                        <li class="list-group-item list-custom d-flex mb-2">
                                                <div class="row mx-0 w-100 py-2">
                                                    <div class="col-12 col-sm-4 mb-3">
                                                        <p class="fz-12 blue-2 mb-0">nombre completo</p>
                                                        <h4 class="fz-18 dark-1 mb-0">{{ $i->nombre }}</h4>
                                                    </div>
                                                    <div class="col-12 col-sm-4 mb-3">
                                                        <p class="fz-12 blue-2 mb-0">dirección</p>
                                                        <h4 class="fz-18 dark-1 mb-0">{{ $i-> direccion}}</h4>
                                                    </div>
                                                    <div class="col-12 col-sm-4 mb-3">
                                                        <p class="fz-12 blue-2 mb-0">teléfono de Contacto</p>
                                                        <h4 class="fz-18 dark-1 mb-0">{{ $i->telefono }}</h4>
                                                    </div>
                                                    <div class="col-12 col-sm-4">
                                                        <p class="fz-12 blue-2 mb-0">Tipo de Sangre</p>
                                                        <h4 class="fz-18 dark-1 mb-0">{{ $i->tipo_sangre }}</h4>
                                                    </div>
                                                    <div class="col-12 col-sm-4">
                                                        <p class="fz-12 blue-2 mb-0">Años de Experiencia</p>
                                                        <h4 class="fz-18 dark-1 mb-0">{{ $i->ano_experiencia }} años</h4>
                                                    </div>
                                                    <div class="col-12 col-sm-4">
                                                        <p class="fz-12 blue-2 mb-0">Fecha de nacimiento</p>
                                                        <h4 class="fz-18 dark-1 mb-0">{{ $i->fecha_nacimiento }}</h4>
                                                    </div>
                                                </div>
                                                <div class="col-1 d-flex justify-content-center align-items-center">
                                                    <a href="/editar_doctor/{{$i->id}}" class="text-decoration-none dark-1">
                                                        <i class="far fa-edit fz-19 cursor-pointer"></i>
                                                    </a>
                                                </div>
                                                <div class="col-1 d-flex justify-content-center align-items-center">
                                                    <a href="/eliminar_doctor/{{$i->id}}" class="text-decoration-none dark-1">
                                                        <i class="far fa-trash-alt fz-19 cursor-pointer"></i>
                                                    </a>
                                                </div>
                                            </li>
                                            @endforeach
                                    </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script src="{{ asset('js/crear_doctor.js') }}" defer></script>

<!-- Modal Crear-->
<div class="modal fade" id="crear-doctor" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" style="border-radius: 23px;">
            <div class="modal-header justify-content-center br-none">
                <h5 class="modal-title text-center proxima-Nova-bold" id="crear-doctorLabel">Crear Doctor</h5>
                <i class="fas fa-times blue fz-20 position-absolute cursor-pointer" style="right: 18px;"
                    data-bs-dismiss="modal" aria-label="Close"></i>
            </div>
            <div class="modal-body px-4 px-sm-5">
                <form action="/crear_doctor" method="POST">
                    @csrf
                    @method('POST')
                    <div class="form-floating mb-3">
                        <input type="text" maxlength="50" class="form-control br-radius-16 px-3 pt-4 pb-2"
                            id="nombreCompletoHos" placeholder="Nombre Completa" name="nombreCompletoHos" required>
                        <label for="nombreCompletoHos" class="dark-1">Nombre completo</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" maxlength="50" class="form-control br-radius-16 px-3 pt-4 pb-2"
                            id="direccionHos" name="direccionHos" placeholder="Dirección" required>
                        <label for="direccionHos" class="dark-1">Dirección</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" maxlength="10" class="form-control br-radius-16 px-3 pt-4 pb-2"
                            id="telefonoContactoHos" name="telefonoContactoHos" placeholder="Telefono contacto" required>
                        <label for="telefonoContactoHos" class="dark-1">Telefono contacto</label>
                    </div>
                    <div class="form-floating mb-3">
                        <select class="form-select br-radius-16 px-3 pt-4 pb-2" id="tipoSangreHos" name="tipoSangreHos"
                            aria-label="Tipo Sangre" placeholder="Tipo Sangre" required>
                            <option value="" selected>Elegir tipo de sangre</option>
                            <option value="O+">O+</option>
                            <option value="O-">O-</option>
                        </select>
                        <label for="tipoSangreHos" class="dark-1">Tipo Sangre</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" maxlength="4" class="form-control br-radius-16 px-3 pt-4 pb-2"
                            id="anoExperienciaHos" name="anoExperienciaHos" placeholder="Año Experiencia" required>
                        <label for="anoExperienciaHos" class="dark-1">Año Experiencia</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="date" class="form-control br-radius-16 px-3 pt-4 pb-2" id="fechaNacimientoHos"
                            name="fechaNacimientoHos" required>
                        <label for="fechaNacimientoHos" class="dark-1">Fecha Nacimiento</label>
                    </div>
                    <div class="modal-footer justify-content-center br-none">
                        <button type="submit"
                            class="fz-20 fw-bold bg-blue white br-none br-radius-16 py-2 px-2 w-300 btn-disabled"
                            id="btn-crearDoctor">Crear</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal Editar-->
{{-- <div class="modal fade" id="editar-doctor" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" style="border-radius: 23px;">
            <div class="modal-header justify-content-center br-none">
                <h5 class="modal-title text-center proxima-Nova-bold" id="editar-doctorLabel">Editar Doctor</h5>
                <i class="fas fa-times blue fz-20 position-absolute cursor-pointer" style="right: 18px;"
                    data-bs-dismiss="modal" aria-label="Close"></i>
            </div>
            <div class="modal-body px-4 px-sm-5">
                <div class="form-floating mb-3">
                    <input type="text" maxlength="50" class="form-control br-radius-16 px-3 pt-4 pb-2"
                        id="nombreCompletoHosEdit" placeholder="Nombre Completa" value="Firulais casicasi">
                    <label for="nombreCompletoHosEdit" class="dark-1">Nombre completo</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" maxlength="50" class="form-control br-radius-16 px-3 pt-4 pb-2"
                        id="direccionHosEdit" placeholder="Dirección" value="cra 25 #14c - 1256">
                    <label for="direccionHosEdit" class="dark-1">Dirección</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" maxlength="10" class="form-control br-radius-16 px-3 pt-4 pb-2"
                        id="telefonoContactoHosEdit" placeholder="Telefono contacto" value="3016716884">
                    <label for="telefonoContactoHosEdit" class="dark-1">Telefono contacto</label>
                </div>
                <div class="form-floating mb-3">
                    <select class="form-select br-radius-16 px-3 pt-4 pb-2" id="tipoSangreHosEdit"
                        aria-label="Tipo Sangre" placeholder="Tipo Sangre" value="O+">
                        <option value="" selected>Elegir tipo de sangre</option>
                        <option value="O+">O+</option>
                        <option value="O-">O-</option>
                    </select>
                    <label for="tipoSangreHosEdit" class="dark-1">Tipo Sangre</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" maxlength="4" class="form-control br-radius-16 px-3 pt-4 pb-2"
                        id="anoExperienciaHosEdit" placeholder="Año Experiencia" value="2004">
                    <label for="anoExperienciaHosEdit" class="dark-1">Año Experiencia</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="date" class="form-control br-radius-16 px-3 pt-4 pb-2" id="fechaNacimientoHosEdit"
                        value="2021-05-03">
                    <label for="fechaNacimientoHosEdit" class="dark-1">Fecha Nacimiento</label>
                </div>
            </div>
            <div class="modal-footer justify-content-center br-none">
                <button type="button"
                    class="fz-20 fw-bold bg-blue white br-none br-radius-16 py-2 px-2 w-300 btn-disabled"
                    id="btn-crearDoctor" data-bs-dismiss="modal" aria-label="Close">Editar</button>
            </div>
        </div>
    </div>
</div>
 --}}

<!-- Modal eliminar -->
{{-- <div class="modal fade" id="eliminar-doctor" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" style="border-radius: 23px;">
            <div class="modal-header justify-content-center br-none">
                <i class="fas fa-times blue fz-20 position-absolute cursor-pointer" style="right: 18px;"
                    data-bs-dismiss="modal" aria-label="Close"></i>
            </div>
            <div class="modal-body px-4 px-sm-5">
                <h2 class="proxima-Nova-Bold dark-1 text-center">Seguro que lo quiere eliminar?</h2>
            </div>
            <div class="modal-footer justify-content-center br-none">
                <button type="button"
                    class="fz-20 fw-bold bg-blue white br-none br-radius-16 py-2 px-2 w-300 btn-disabled"
                    id="btn-eliminarDoctor" data-bs-dismiss="modal" aria-label="Close">Eliminar</button>
            </div>
        </div>
    </div>
</div>
 --}}