@extends('layouts.app')

@section('title')
    Admin
@endsection


@section('content')
  <div class="h-100 d-flex flex-column">
    <div class="container-fluid flex-1-0-auto bg-gradian-blue-lit linea-blue position-relative d-flex overflow-auto">
      <div class="container w-1450 d-flex justify-content-center align-items-center position-relative z-index-99 flex-column pb-100">
        <h1 class="fz-40 fz-md-60 white fw-bold text-center mb-4 mt-50 Jellee-Roman mb-3">Admin</h1>
        <div class="position-relative w-1600">
             <a href="./" class="text-decoration-none"><i class="fas fa-caret-left fz-60 white position-absolute icon-atras"></i></a>
        </div>
        <div class="row w-100 d-flex justify-content-center">
          <div class="col-md-6 col-lg-5 col-xl-4 mb-3 px-4">
            <a href="/hospital" class="text-decoration-none">
              <div class="bg-white br-radius-16 shadow--1 px-0 py-5 d-flex justify-content-center align-items-center flex-column position-relative linea-2 overflow-hidden">
                <div class="w-100 position-relative d-flex justify-content-center">
                  <div class="circle-aff">
                    <div class="circle d-flex justify-content-center align-items-center shadow--1 position-relative">
                      <i class="fas fa-hospital-user blue fz-80"></i>
                    </div>
                  </div>
                </div>
                <h1 class="fz-48 proxima-Nova-Bold  fw-bold dark-1 mb-3 text-center mt-5">Hospital</h1>
              </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-5 col-xl-4 mb-3 px-4">
            <a href="/doctorC" class="text-decoration-none">
              <div class="bg-white br-radius-16 shadow--1 px-0 py-5 d-flex justify-content-center align-items-center flex-column position-relative linea-2 overflow-hidden">
                <div class="w-100 position-relative d-flex justify-content-center">
                  <div class="circle-aff">
                    <div class="circle d-flex justify-content-center align-items-center shadow--1 position-relative">
                      <i class="fas fa-stethoscope blue fz-80"></i>
                    </div>
                  </div>
                </div>
                <h1 class="fz-48 proxima-Nova-Bold fw-bold dark-1 mb-3 text-center mt-5">Doctor</h1>
              </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-5 col-xl-4 mb-3 px-4">
            <a href="/usuario" class="text-decoration-none">
              <div class="bg-white br-radius-16 shadow--1 px-0 py-5 d-flex justify-content-center align-items-center flex-column position-relative linea-2 overflow-hidden">
                <div class="w-100 position-relative d-flex justify-content-center">
                  <div class="circle-aff">
                    <div class="circle d-flex justify-content-center align-items-center shadow--1 position-relative">
                      <i class="fas fa-user-nurse blue fz-80"></i>
                    </div>
                  </div>
                </div>
                <h1 class="fz-48 proxima-Nova-Bold dark-1 mb-3 text-center mt-5">Usuario</h1>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection