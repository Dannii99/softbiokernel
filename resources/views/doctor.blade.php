@extends('layouts.app')

@section('title')
    Doctor
@endsection


@section('content')
    <div class="h-100 d-flex flex-column">
        <div class="container-fluid flex-1-0-auto bg-gradian-blue-lit linea-blue position-relative d-flex">
            <div
                class="container w-1450 d-flex justify-content-center align-items-center position-relative z-index-99 flex-column pb-100">
                <h1 class="fz-40 fz-md-60 white fw-bold text-center mb-4 mt-50 Jellee-Roman mb-3">Doctor</h1>
                <div class="row w-100 d-flex justify-content-center">
                    <div class="col-md-8 mb-3 mb-md-0 px-4">
                        <div class="position-relative w-910">
                            <a href="./" class="text-decoration-none"><i
                                    class="fas fa-caret-left fz-60 white position-absolute icon-atras"></i></a>
                        </div>
                        <div
                            class="bg-white br-radius-16 shadow--1 px-0 py-5 d-flex justify-content-center align-items-center flex-column position-relative linea-2 overflow-hidden">
                            <div class="w-100 position-relative d-flex justify-content-center">
                                <div class="circle-aff">
                                    <div
                                        class="circle d-flex justify-content-center align-items-center shadow--1 position-relative">
                                        <i class="fas fa-user-md  blue fz-80"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100 shadow--1 py-3 px-3 mt-50">
                                <h1 class="fz-35 proxima-Nova-Bold dark-1 mb-0 text-center">Seleccionar Doctores</h1>
                            </div>
                            <div class="w-100 px-3">
                                @if (isset($doctor) && count($doctor) != 0)
                                    <ul class="list-group list-group-flush px-0 mt-4">
                                        @foreach ($doctor as $count => $i)
                                            <li class="list-group-item list-custom d-flex mb-3">
                                                <div class="row mx-0 w-100 py-2">
                                                    <div class="col-12 col-sm-4 mb-3">
                                                        <p class="fz-12 blue-2 mb-0">nombre completo</p>
                                                        <h4 class="fz-18 dark-1 mb-0">{{ $i->nombre }}</h4>
                                                    </div>
                                                    <div class="col-12 col-sm-4 mb-3">
                                                        <p class="fz-12 blue-2 mb-0">dirección</p>
                                                        <h4 class="fz-18 dark-1 mb-0">{{ $i->direccion }}</h4>
                                                    </div>
                                                    <div class="col-12 col-sm-4 mb-3">
                                                        <p class="fz-12 blue-2 mb-0">teléfono de Contacto</p>
                                                        <h4 class="fz-18 dark-1 mb-0">{{ $i->telefono }}</h4>
                                                    </div>
                                                    <div class="col-12 col-sm-4">
                                                        <p class="fz-12 blue-2 mb-0">Tipo de Sangre</p>
                                                        <h4 class="fz-18 dark-1 mb-0">{{ $i->tipo_sangre }}</h4>
                                                    </div>
                                                    <div class="col-12 col-sm-4">
                                                        <p class="fz-12 blue-2 mb-0">Años de Experiencia</p>
                                                        <h4 class="fz-18 dark-1 mb-0">{{ $i->ano_experiencia }} años</h4>
                                                    </div>
                                                    <div class="col-12 col-sm-4">
                                                        <p class="fz-12 blue-2 mb-0">Fecha de nacimiento</p>
                                                        <h4 class="fz-18 dark-1 mb-0">{{ $i->fecha_nacimiento }}</h4>
                                                    </div>
                                                </div>
                                                <div class="col-1 d-flex justify-content-center align-items-center">
                                                    <input class="form-check-input radio-doctor" type="radio"
                                                        name="doctores" id="doctor-1">
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <div class="w-100 d-flex justify-content-center px-4 pt-2">
                                <button
                                    class="fz-20 fw-bold bg-blue white br-none br-radius-16 py-3 px-2 w-450 btn-disabled"
                                    id="btn-doctor" disabled data-bs-toggle="modal"
                                    data-bs-target="#verificar-usuario">Aceptar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script src="{{ asset('js/doctor.js') }}" defer></script>

<!-- Modal Verificar Usuario-->
<div class="modal fade" id="verificar-usuario" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" style="border-radius: 23px;">
            <div class="modal-header justify-content-center br-none">
                <h5 class="modal-title text-center proxima-Nova-bold" id="verificar-usuarioLabel">Digitar Documente de Usuario</h5>
                <i class="fas fa-times blue fz-20 position-absolute cursor-pointer" style="right: 18px;" data-bs-dismiss="modal"></i>
            </div>
            <form action="consulta" method="POST">
                @csrf
                @method('POST')
                <div class="modal-body py-2 px-4 px-sm-5">
                    <div class="form-floating">
                        <input type="text" maxlength="10" class="form-control br-radius-16 px-3 pt-4 pb-2"
                            id="cedulaUsu" name="cedulaUsu" placeholder="Cedula" required>
                        <label for="cedulaUsu" class="dark-1">Cedula Ciudadania</label>
                    </div>
                </div>
                <div class="modal-footer justify-content-center br-none">
                    <button type="submit"
                        class="fz-20 fw-bold bg-blue white br-none br-radius-16 py-2 px-2 w-300 mb-3 btn-disabled"
                        id="btn-consulClient">Consultar</button>
                </div>
            </form>
        </div>
    </div>
</div>
