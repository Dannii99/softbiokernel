@extends('layouts.app')

@section('title')
    Editar Doctor
@endsection


@section('content')
    <div class="h-100 d-flex flex-column">
        <div class="container-fluid flex-1-0-auto bg-gradian-blue-lit linea-blue position-relative d-flex">
            <div
                class="container w-1450 d-flex justify-content-center align-items-center position-relative z-index-99 flex-column pb-100">
                <h1 class="fz-40 fz-md-60 white fw-bold text-center mb-4 mt-50 Jellee-Roman mb-3">Doctor</h1>
                <div class="row w-100 d-flex justify-content-center">
                    <div class="col-md-8 mb-3 mb-md-0 px-4">
                        <div class="position-relative w-910">
                            <a href="/doctorC" class="text-decoration-none"><i
                                    class="fas fa-caret-left fz-60 white position-absolute icon-atras"></i></a>
                        </div>
                        <div
                            class="bg-white br-radius-16 shadow--1 px-0 py-5 d-flex justify-content-center align-items-center flex-column position-relative linea-2 overflow-hidden">
                            <div class="w-100 position-relative d-flex justify-content-center">
                                <div class="circle-aff">
                                    <div
                                        class="circle d-flex justify-content-center align-items-center shadow--1 position-relative">
                                        <i class="fas fa-stethoscope blue fz-80"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100 shadow--1 py-3 px-3 mt-50">
                                <h1 class="fz-35 proxima-Nova-Bold dark-1 mb-0 text-center">Editar Doctor</h1>
                            </div>
                            <div class="w-100 px-3 pt-4">
                              @if (isset($doctor) && !empty($doctor))
                                <form action="{{$doctor->id}}" method="post" class="mb-0">
                                  @csrf
                                  @method('PUT')
                                <div class="form-floating mb-3">
                                    <input type="text" maxlength="50" class="form-control br-radius-16 px-3 pt-4 pb-2"
                                        id="nombreCompletoHosEdit" placeholder="Nombre Completa" name="nombreCompletoHosEdit" value="{{$doctor->nombre}}" required>
                                    <label for="nombreCompletoHosEdit" class="dark-1">Nombre completo</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" maxlength="50" class="form-control br-radius-16 px-3 pt-4 pb-2"
                                        id="direccionHosEdit" placeholder="Dirección" name="direccionHosEdit" value="{{$doctor->direccion}}" required>
                                    <label for="direccionHosEdit" class="dark-1">Dirección</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" maxlength="10" class="form-control br-radius-16 px-3 pt-4 pb-2"
                                        id="telefonoContactoHosEdit" placeholder="Telefono contacto" name="telefonoContactoHosEdit" value="{{$doctor->telefono}}" required>
                                    <label for="telefonoContactoHosEdit" class="dark-1">Telefono contacto</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <select class="form-select br-radius-16 px-3 pt-4 pb-2" id="tipoSangreHosEdit"
                                        aria-label="Tipo Sangre" placeholder="Tipo Sangre" name="tipoSangreHosEdit" value="{{$doctor->tipo_sangre}}" required>
                                        <option value="" selected>Elegir tipo de sangre</option>
                                        <option value="O+">O+</option>
                                        <option value="O-">O-</option>
                                    </select>
                                    <label for="tipoSangreHosEdit" class="dark-1">Tipo Sangre</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="text" maxlength="4" class="form-control br-radius-16 px-3 pt-4 pb-2"
                                        id="anoExperienciaHosEdit" placeholder="Año Experiencia" name="anoExperienciaHosEdit" value="{{$doctor->ano_experiencia}}" required>
                                    <label for="anoExperienciaHosEdit" class="dark-1">Año Experiencia</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input type="date" class="form-control br-radius-16 px-3 pt-4 pb-2" id="fechaNacimientoHosEdit"
                                    name="fechaNacimientoHosEdit" value="{{$doctor->fecha_nacimiento}}" required>
                                    <label for="fechaNacimientoHosEdit" class="dark-1">Fecha Nacimiento</label>
                                </div>
                                <div class="d-flex justify-content-center">
                                  <button type="submit"
                                  class="fz-20 fw-bold bg-blue white br-none br-radius-16 py-2 px-2 w-300 btn-disabled"
                                  id="btn-editarDoctor">Editar</button>
                                </div>
                              </form>
                              @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script src="{{ asset('js/editar_doctor.js') }}" defer></script>
