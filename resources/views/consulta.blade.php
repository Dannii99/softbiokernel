@extends('layouts.app')

@section('title')
    Consulta
@endsection


@section('content')
    <div class="h-100 d-flex flex-column">
        <div class="container-fluid flex-1-0-auto bg-gradian-blue-lit linea-blue position-relative d-flex">
            <div
                class="container w-1450 d-flex justify-content-center align-items-center position-relative z-index-99 flex-column pb-100">
                <h1 class="fz-40 fz-md-60 white fw-bold text-center mb-4 mt-50 Jellee-Roman mb-3">Consulta</h1>
                <div class="row w-100 d-flex justify-content-center">
                    <div class="col-md-8 mb-3 mb-md-0 px-4">
                        <div class="position-relative w-910">
                            <a href="./doctor" class="text-decoration-none"><i
                                    class="fas fa-caret-left fz-60 white position-absolute icon-atras"></i></a>
                        </div>
                        <div
                            class="bg-white br-radius-16 shadow--1 px-0 py-5 d-flex justify-content-center align-items-center flex-column position-relative linea-2 overflow-hidden">
                            <div class="w-100 position-relative d-flex justify-content-center">
                                <div class="circle-aff">
                                    <div
                                        class="circle d-flex justify-content-center align-items-center shadow--1 position-relative">
                                        <i class="fas fa-notes-medical blue fz-80"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100 shadow--1 py-3 px-3 mt-50">
                                <h1 class="fz-35 proxima-Nova-Bold dark-1 mb-0 text-center">Datos Paciente</h1>
                            </div>
                            <div class="w-100 px-3 pt-5">
                                <form action="{{ $mostrar ? "/editar_cliente/$cliente->id" : '/crear_usuario_com' }}" method="POST">
                                    @csrf
                                    <div class="row g-2 mb-3">
                                        <div class="col-md">
                                            <div class="form-floating">
                                                <input type="text" maxlength="50"
                                                    class="form-control br-radius-16 px-3 pt-4 pb-2" id="nombreCompleto"
                                                    value="{{ $mostrar ? $cliente->nombre : '' }}"
                                                    placeholder="Nombre Completo" name="nombreCompleto" required>
                                                <label for="nombreUsuario" class="dark-1">Nombre Completo</label>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-floating">
                                                <select class="form-select br-radius-16 px-3 pt-4 pb-2" id="epsAsociado"
                                                    name="epsAsociado" onchange="consulValid()" required
                                                    aria-label="EPS Asociado" placeholder="EPS Asociado">
                                                    <option value="" selected>Elige la EPS</option>
                                                    <option
                                                        {{ $mostrar ? ($cliente->eps == 'SURA' ? 'selected' : '') : '' }}
                                                        value="SURA">SURA</option>
                                                    <option
                                                        {{ $mostrar ? ($cliente->eps == 'COOMEVA' ? 'selected' : '') : '' }}
                                                        value="COOMEVA">COOMEVA</option>
                                                    <option
                                                        {{ $mostrar ? ($cliente->eps == 'SUSALUD' ? 'selected' : '') : '' }}
                                                        value="SUSALUD">SUSALUD</option>
                                                    <option
                                                        {{ $mostrar ? ($cliente->eps == 'CAFESALUD' ? 'selected' : '') : '' }}
                                                        value="CAFESALUD">CAFESALUD</option>
                                                    <option
                                                        {{ $mostrar ? ($cliente->eps == 'VIVA1A' ? 'selected' : '') : '' }}
                                                        value="VIVA1A">VIVA1A</option>
                                                </select>
                                                <label for="epsAsociado" class="dark-1">EPS Asociado</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-2 mb-3">
                                        <div class="col-md">
                                            <div class="form-floating">
                                                <input type="text" maxlength="10"
                                                    value="{{ $mostrar ? $cliente->telefono : '' }}"
                                                    class="form-control br-radius-16 px-3 pt-4 pb-2" id="telefono"
                                                    name="telefono" placeholder="telefono" onkeyup="consulValid()" required>
                                                <label for="telefono" class="dark-1">telefono</label>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-floating">
                                                <input type="text" maxlength="10"
                                                    value="{{ $mostrar ? $cliente->cedula : '' }}"
                                                    class="form-control br-radius-16 px-3 pt-4 pb-2" id="cedulaUsuario"
                                                    name="cedulaUsuario" placeholder="cedula" onkeyup="consulValid()"
                                                    required>
                                                <label for="cedulaUsuario" class="dark-1">Cedula Ciudadania</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-2 mb-3">
                                        <div class="col-md">
                                            <div class="form-floating">
                                                <input type="text" maxlength="10"
                                                    value="{{ $mostrar ? $cliente->telefono_acompañante : '' }}"
                                                    class="form-control br-radius-16 px-3 pt-4 pb-2"
                                                    id="TelefonoAcompañante" name="TelefonoAcompañante"
                                                    placeholder="Teléfono del Acompañante" onkeyup="consulValid()" required>
                                                <label for="TelefonoAcompañante" class="dark-1">Teléfono del
                                                    Acompañante</label>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="col-md">
                                                <div class="form-floating">
                                                    <input type="text" maxlength="50"
                                                        value="{{ $mostrar ? $cliente->nombre_acompañante : '' }}"
                                                        class="form-control br-radius-16 px-3 pt-4 pb-2"
                                                        id="nombreAcompañante" name="nombreAcompañante"
                                                        placeholder="Nombre del Acompañante" onkeyup="consulValid()"
                                                        required>
                                                    <label for="nombreAcompañante" class="dark-1">Nombre del
                                                        Acompañante</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-2 mb-3">
                                        <div class="col-md-6">
                                            <div class="form-floating">
                                                <input type="text" maxlength="50"
                                                    value="{{ $mostrar ? $cliente->persona_contacto : '' }}"
                                                    class="form-control br-radius-16 px-3 pt-4 pb-2" id="persona_contacto"
                                                    name="personaContacto" placeholder="Persona Contacto"
                                                    onkeyup="consulValid()" required>
                                                <label for="persona_contacto" class="dark-1">Persona Contacto</label>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-floating">
                                                <input type="text" maxlength="50"
                                                    value="{{ $mostrar ? $cliente->direccion : '' }}"
                                                    class="form-control br-radius-16 px-3 pt-4 pb-2" id="direccionPaciente"
                                                    name="direccionPaciente" placeholder="Direccion del Paciente"
                                                    onkeyup="consulValid()" required>
                                                <label for="direccionPaciente" class="dark-1">Dirección del Paciente</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-2 mb-3">
                                        <div class="col-md">
                                            <div class="form-floating">
                                                <select class="form-select br-radius-16 px-3 pt-4 pb-2"
                                                    id="antecedentesMedicos" name="antecedentesMedicos"
                                                    aria-label="Antecedentes médicos" placeholder="Antecedentes médicos"
                                                    onchange="consulValid()" required>
                                                    <option value="" selected>Elige opción</option>
                                                    <option
                                                        {{ $mostrar ? ($cliente->antecedente_medico == 'si' ? 'selected' : '') : '' }}
                                                        value="si">Si</option>
                                                    <option
                                                        {{ $mostrar ? ($cliente->antecedente_medico == 'no' ? 'selected' : '') : '' }}
                                                        value="no">No</option>
                                                </select>
                                                <label for="antecedentesMedicos" class="dark-1">Antecedentes médicos</label>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="col-md">
                                                <div class="form-floating">
                                                    <input type="text"
                                                        value="{{ $mostrar ? $cliente->registrar_antecedentes : '' }}"
                                                        maxlength="50" class="form-control br-radius-16 px-3 pt-4 pb-2"
                                                        id="antecedente" name="antecedente" placeholder="antecedente"
                                                        onkeyup="consulValid()">
                                                    <label for="antecedente" class="dark-1">Registrar antecedente</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row g-2 mb-3">
                                        <div class="col-md">
                                            <div class="col-md">
                                                <div class="form-floating">
                                                    <input type="text"
                                                        value="{{ $mostrar ? $cliente->motivos_consulta : '' }}"
                                                        maxlength="150" class="form-control br-radius-16 px-3 pt-4 pb-2"
                                                        id="motivosConsulta" name="motivosConsulta"
                                                        placeholder="Motivos de la consulta" onkeyup="consulValid()"
                                                        required>
                                                    <label for="motivosConsulta" class="dark-1">Motivos de la
                                                        consulta</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            @if ($mostrar)
                                <div class="w-100 d-flex justify-content-center px-4 pt-2">
                                    <button type="submit"
                                        class="fz-20 fw-bold bg-blue white br-none br-radius-16 py-3 px-2 w-450 btn-disabled"
                                        id="btn-consultar"
                                        onclick="window.location.href = 'test_covid/{{ $cliente->cedula }}'"
                                        disabled>Siguiente</button>
                                @else
                                    <button type="submit"
                                        class="fz-20 fw-bold bg-blue white br-none br-radius-16 py-3 px-2 w-450 btn-disabled"
                                        id="btn-registrar">Registrar</button>
                            @endif
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
<script src="{{ asset('js/consulta.js') }}" defer></script>
