@extends('layouts.app')

@section('title')
    Consulta
@endsection


@section('content')
    <div class="h-100 d-flex flex-column">
        <div class="container-fluid flex-1-0-auto bg-gradian-blue-lit linea-blue position-relative d-flex">
            <div
                class="container w-1450 d-flex justify-content-center align-items-center position-relative z-index-99 flex-column pb-100">
                <h1 class="fz-40 fz-md-60 white fw-bold text-center mb-4 mt-50 Jellee-Roman mb-3">Test Covid</h1>
                <div class="row w-100 d-flex justify-content-center">
                    <div class="col-md-8 mb-3 mb-md-0 px-4">
                        <div class="position-relative w-910">
                            <a href="./consulta" class="text-decoration-none"><i
                                    class="fas fa-caret-left fz-60 white position-absolute icon-atras"></i></a>
                        </div>
                        <div
                            class="bg-white br-radius-16 shadow--1 px-0 py-5 d-flex justify-content-center align-items-center flex-column position-relative linea-2 overflow-hidden">
                            <div class="w-100 position-relative d-flex justify-content-center">
                                <div class="circle-aff">
                                    <div
                                        class="circle d-flex justify-content-center align-items-center shadow--1 position-relative">
                                        <i class="fas fa-notes-medical blue fz-80"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100 shadow--1 py-3 px-3 mt-50">
                                <h1 class="fz-35 proxima-Nova-Bold dark-1 mb-0 text-center">Formulario</h1>
                            </div>
                            <div class="w-100 px-3 px-sm-5 pt-5">
                                <form action="/submit_test/{{ $cliente->cedula }}" method="POST">
                                    @csrf
                                    <div class="d-flex justify-content-between mb-4">
                                        <label class="form-check-label" for="tos">El paciente prensenta Tos?</label>
                                        <div class="form-check form-switch ms-2">
                                            <input  class="form-check-input" type="checkbox" id="tos" name="1">
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-between mb-4">
                                        <label class="form-check-label" for="dificultad-respiratoria">El paciente prensenta
                                            dificultad para respirar (sentir que le falta el aire)?</label>
                                        <div class="form-check form-switch ms-2">
                                            <input  class="form-check-input" type="checkbox" id="dificultad_respiratoria" name="2">
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-between mb-4">
                                        <label class="form-check-label" for="fiebre">El paciente prensenta fiebre?</label>
                                        <div class="form-check form-switch ms-2">
                                            <input  class="form-check-input" type="checkbox" id="fiebre" name="3">
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-between mb-4">
                                        <label class="form-check-label" for="escalofrios">El paciente prensenta
                                            escalofríos?</label>
                                        <div class="form-check form-switch ms-2">
                                            <input  class="form-check-input" type="checkbox" id="escalofrios" name="4">
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between mb-4">
                                        <label class="form-check-label" for="temblores">El paciente prensenta temblores y
                                            escalofríos que no ceden?
                                        </label>
                                        <div class="form-check form-switch ms-2">
                                            <input  class="form-check-input" type="checkbox" id="temblores" name="5">
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between mb-4">
                                        <label class="form-check-label" for="dolor-muscular">El paciente prensenta dolor
                                            muscular?</label>
                                        <div class="form-check form-switch ms-2">
                                            <input  class="form-check-input" type="checkbox" id="dolor_muscular" name="6">
                                        </div>
                                    </div>
                            </div>
                            <div class="w-100 d-flex justify-content-center px-4 pt-2">
                                <button type="submit"
                                    class="fz-20 fw-bold bg-blue white br-none br-radius-16 py-3 px-2 w-450 btn-disabled"
                                    id="btn-conCovid" >Siguiente</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script src="{{ asset('js/test_covid.js') }}" defer></script>