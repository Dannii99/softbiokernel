@extends('layouts.app')

@section('title')
    Resultados
@endsection


@section('content')
  <div class="h-100 d-flex flex-column" id="resultados">
    <div class="container-fluid flex-1-0-auto bg-gradian-blue-lit linea-blue position-relative d-flex">
      <div class="container w-1145  d-flex justify-content-center align-items-center position-relative z-index-99 flex-column pb-100">
        <h1 class="fz-40 fz-md-60 white fw-bold text-center mb-4 mt-50 Jellee-Roman mb-3">Resultado</h1>
        <div class="row w-100 d-flex justify-content-center">
          <div class="col-md-8 mb-3 mb-md-0 px-4">
            <div class="position-relative w-910">
             <a href="/doctor" class="text-decoration-none"><i class="fas fa-caret-left fz-60 white position-absolute icon-atras"></i></a>
            </div>
            <div class="bg-white br-radius-16 shadow--1 px-0 pt-5 pb-4 d-flex justify-content-center align-items-center flex-column position-relative linea-2 overflow-hidden">
              <div class="w-100 position-relative d-flex justify-content-center">
                <div class="circle-aff">
                  <div class="circle d-flex justify-content-center align-items-center shadow--1 position-relative">
                   <i class="fas fa-syringe blue fz-60"></i>
                  </div>
                </div>
              </div>
              <div class="w-100 px-3">
               
                <h1 class="dark-1 fz-40 proxima-Nova-Bold text-center mt-5">{{$diagnostico}}</h1>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection