
@extends('layouts.app')

@section('title')
    Crear Hospital
@endsection


@section('content')
  <div class="h-100 d-flex flex-column">
    <div class="container-fluid flex-1-0-auto bg-gradian-blue-lit linea-blue position-relative d-flex">
      <div class="container w-1450 d-flex justify-content-center align-items-center position-relative z-index-99 flex-column pb-100">
        <h1 class="fz-40 fz-md-60 white fw-bold text-center mb-4 mt-50 Jellee-Roman mb-3">Hospital</h1>
        <div class="row w-100 d-flex justify-content-center">
          <div class="col-md-8 mb-3 mb-md-0 px-4">
            <div class="position-relative w-910">
             <a href="./admin" class="text-decoration-none"><i class="fas fa-caret-left fz-60 white position-absolute icon-atras"></i></a>
            </div>
            <div class="bg-white br-radius-16 shadow--1 px-0 pt-5 pb-4 d-flex justify-content-center align-items-center flex-column position-relative linea-2 overflow-hidden">
              <div class="w-100 position-relative d-flex justify-content-center">
                <div class="circle-aff">
                  <div class="circle d-flex justify-content-center align-items-center shadow--1 position-relative">
                    <i class="fas fa-hospital-user blue fz-80"></i>
                  </div>
                </div>
              </div>
              <div class="w-100 shadow--1 py-3 px-3 mt-50">
                <h1 class="fz-35 proxima-Nova-Bold dark-1 mb-0 text-center">Crear hospital</h1>
              </div>
              <div class="w-100 px-3">
                @if(count($hospital) == 0)
                  <button type="button" class="fz-18 fw-bold br-none bg-blue white br-radius-16 px-5 py-2 mt-3 shadow--1 cursor-pointer" data-bs-toggle="modal"   data-bs-target="#crear-hospital">Crear <i class="fas fa-plus ms-2"></i>
                  </button>
                @endif
                @if(!empty($hospital))
                <ul class="list-group list-group-flush px-0 mt-4">
                  @foreach ($hospital as $item)
                    <li class="list-group-item list-custom mb-2">
                      <div class="row mx-0 w-100 py-2">
                        <div class="col">
                          <p class="fz-12 blue-2 mb-0">nombre hospital</p>
                          <h4 class="fz-23 dark-1 mb-0">{{$item->nombre}}</h4>
                        </div>
                       {{--  <div class="col-1 d-flex justify-content-center align-items-center">
                          <i class="far fa-edit fz-19 cursor-pointer" disabled data-bs-toggle="modal" data-bs-target="#editar-hospital"></i>
                        </div> --}}
                        <div class="col-1 d-flex justify-content-center align-items-center">
                          <a href="/elimiar_hospital/{{$item->id}}" class="text-decoration-none dark-1">
                            <i class="far fa-trash-alt fz-19 cursor-pointer"></i>
                          </a>
                        </div>
                      </div>
                    </li>
                  @endforeach
                </ul>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
  <script src="{{ asset('js/hospital.js') }}" defer></script>
  
<!-- Modal Crear-->
<div class="modal fade" id="crear-hospital" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content" style="border-radius: 23px;">
      <div class="modal-header justify-content-center br-none">
        <h5 class="modal-title text-center proxima-Nova-bold" id="crear-hospitalLabel">Crear Hospital</h5>
        <i class="fas fa-times blue fz-20 position-absolute cursor-pointer" style="right: 18px;" data-bs-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body px-4 px-sm-5">
        <form action="/crear_hospital" method="POST">
          @csrf
          <div class="form-floating">
            <input type="text" maxlength="50" class="form-control br-radius-16 px-3 pt-4 pb-2" id="nombreHospital" placeholder="Nombre Hospital" name="nombreHospital" required>
            <label for="nombreHospital" class="dark-1">Nombre Hospital</label>
          </div>
          <div class="modal-footer justify-content-center br-none">
            <button type="submit" class="fz-20 fw-bold bg-blue white br-none br-radius-16 py-2 px-2 w-300 btn-disabled" id="btn-crearHospital">Crear</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<!-- Modal Crear-->
<div class="modal fade" id="editar-hospital" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content" style="border-radius: 23px;">
      <div class="modal-header justify-content-center br-none">
        <h5 class="modal-title text-center proxima-Nova-bold" id="editar-hospitalLabel">Editar Hospital</h5>
        <i class="fas fa-times blue fz-20 position-absolute cursor-pointer" style="right: 18px;" data-bs-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body px-4 px-sm-5">
        <div class="form-floating">
          <input type="text" maxlength="50" class="form-control br-radius-16 px-3 pt-4 pb-2" id="nombreHospitalEdit" placeholder="Nombre Hospital" value="Hospital del Nortel">
          <label for="nombreHospitalEdit" class="dark-1">Nombre Hospital</label>
        </div>
      </div>
      <div class="modal-footer justify-content-center br-none">
        <button type="button" class="fz-20 fw-bold bg-blue white br-none br-radius-16 py-2 px-2 w-300 btn-disabled" id="btn-crearHospital" data-bs-dismiss="modal" aria-label="Close">Editar</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal eliminar -->
<div class="modal fade" id="eliminar-hospital" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content" style="border-radius: 23px;">
      <div class="modal-header justify-content-center br-none">
        <i class="fas fa-times blue fz-20 position-absolute cursor-pointer" style="right: 18px;" data-bs-dismiss="modal" aria-label="Close"></i>
      </div>
      <div class="modal-body px-4 px-sm-5">
        <h2 class="proxima-Nova-Bold dark-1 text-center">Seguro que lo quiere eliminar?</h2>
      </div>
      <div class="modal-footer justify-content-center br-none">
        <button type="button" class="fz-20 fw-bold bg-blue white br-none br-radius-16 py-2 px-2 w-300 btn-disabled" id="btn-eliminarHospital">Eliminar</button>
      </div>
    </div>
  </div>
</div>