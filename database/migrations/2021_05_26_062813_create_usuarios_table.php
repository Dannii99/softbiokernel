<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('cedula');
            $table->string('direccion');
            $table->string('telefono');
            $table->string('persona_contacto');

            $table->string('eps');
            $table->string('telefono_acompañante');
            $table->string('nombre_acompañante');
            $table->string('antecedente_medico');
            $table->string('registrar_antecedentes');
            $table->string('motivos_consulta');
            $table->string('diagnostico');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
