<?php

use App\Http\Controllers\DoctorController;
use App\Http\Controllers\HospitalController;
use App\Http\Controllers\UsuarioController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/admin', function () {
    return view('admin');
});

/* Route::resource('hospital', HospitalController::class); */

Route::get('/hospital', [HospitalController::class, 'index']);

Route::post('/crear_hospital', [HospitalController::class, 'create']);

Route::get('/elimiar_hospital/{id}', [HospitalController::class, 'destroy']);




Route::get('/doctorC', [DoctorController::class, 'index']);

Route::post('/crear_doctor', [DoctorController::class, 'create']);

Route::get('/editar_doctor/{id}', [DoctorController::class, 'imprimirDoc']);

Route::put('/editar_doctor/{id}', [DoctorController::class, 'edit']);

Route::get('/eliminar_doctor/{id}', [DoctorController::class, 'destroy']);




Route::get('/usuario', [UsuarioController::class, 'index']);

Route::post('/crear_usuario', [UsuarioController::class, 'create']);

Route::post('/crear_usuario_com', [UsuarioController::class, 'createComplete']);

Route::get('/editar_usuario/{id}', [UsuarioController::class, 'imprimirUsu']);

Route::put('/editar_usuario/{id}', [UsuarioController::class, 'edit']);

Route::post('/editar_cliente/{id}', [UsuarioController::class, 'update']);

Route::get('/eliminar_usuario/{id}', [UsuarioController::class, 'destroy']);



Route::get('/doctor', [DoctorController::class, 'index2']);


/* Route::get('/doctor', function () {
    return view('doctor');
});
 */
Route::post('/consulta', [UsuarioController::class, 'showConsult']);

Route::get('/test_covid/{documento}', [UsuarioController::class, 'showTest']);

Route::post('/submit_test/{documento}', [UsuarioController::class, 'submitTest']);


Route::get('/resultados', function () {
    return view('resultado');
});


/* Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home'); */

