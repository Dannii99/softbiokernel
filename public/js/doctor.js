btnDoctor = document.getElementById("btn-doctor");
radio = [...document.getElementsByClassName("radio-doctor")];

radio.forEach((x) => {
    x.addEventListener("click", () => {
        btnDoctor.removeAttribute("disabled");
    });
});


/* btnConsulClient = document.getElementById("btn-consulClient");
 */
/* btnConsulClient.addEventListener("click", function(){ 
    window.location.pathname = "consulta"
}); */



function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
      textbox.addEventListener(event, function() {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }
      });
    });
  }

  // Only numbers
    setInputFilter(document.getElementById("cedulaUsu"), function(value) {
      return /^-?\d*$/.test(value); });