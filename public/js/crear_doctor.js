
/* var btnCrearDoctor = document.getElementById('btn-crearDoctor');

btnCrearDoctor.addEventListener("click", function(){ 
  var nombreCompletoHos = document.getElementById('nombreCompletoHos').value;
  var direccionHos = document.getElementById('direccionHos').value;
  var telefonoContactoHos = document.getElementById('telefonoContactoHos').value;
  var tipoSangreHos = document.getElementById('tipoSangreHos').value;
  var anoExperienciaHos = document.getElementById('anoExperienciaHos').value;
  var fechaNacimientoHos = document.getElementById('fechaNacimientoHos').value;
  limpiar();
});

function limpiar() {
  nombreCompletoHos.value = "";
  direccionHos.value = "";
  telefonoContactoHos.value = "";
  tipoSangreHos.value = "";
  anoExperienciaHos.value = "";
  fechaNacimientoHos.value = "";
}
 */

function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}

  // Only number.
  setInputFilter(document.getElementById("anoExperienciaHos"), function(value) {
    return /^-?\d*$/.test(value); });
  setInputFilter(document.getElementById("telefonoContactoHos"), function(value) {
    return /^-?\d*$/.test(value); });

// Only letter
  setInputFilter(document.getElementById("nombreCompletoHos"), function(value) {
    return /^[[A-Za-z\s]*$/i.test(value); });

