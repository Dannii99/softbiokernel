var btnCrearHospital = document.getElementById('btn-crearHospital');

/* btnCrearHospital.addEventListener("click", function(){ 
  var nombreHospital = document.getElementById('nombreHospital').value;
  limpiar();
});

function limpiar() {
  nombreHospital.value = "";
} */

function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}

// Only letter
  setInputFilter(document.getElementById("nombreHospital"), function(value) {
    return /^[[A-Za-z\s]*$/i.test(value); });
