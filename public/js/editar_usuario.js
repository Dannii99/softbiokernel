
function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}

  // Only number.
  setInputFilter(document.getElementById("telefonoContactoUsuEdit"), function(value) {
    return /^-?\d*$/.test(value); });
  setInputFilter(document.getElementById("cedulaUsuEdit"), function(value) {
    return /^-?\d*$/.test(value); });


// Only letter
  setInputFilter(document.getElementById("nombreCompletoUsuEdit"), function(value) {
    return /^[[A-Za-z\s]*$/i.test(value); });
  setInputFilter(document.getElementById("personaContactoUsuEdit"), function(value) {
    return /^[[A-Za-z\s]*$/i.test(value); });


