/* var btnCrearUsuario = document.getElementById('btn-crearUsuario');

btnCrearUsuario.addEventListener("click", function(){ 
  var nombreCompletoUsu = document.getElementById('nombreCompletoUsu').value;
  var direccionUsu = document.getElementById('direccionUsu').value;
  var telefonoContactoUsu = document.getElementById('telefonoContactoUsu').value;
  var personaContactoUsu = document.getElementById('personaContactoUsu').value;
  limpiar();
});

function limpiar() {
  nombreCompletoUsu.value = "";
  direccionUsu.value = "";
  telefonoContactoUsu.value = "";
  personaContactoUsu.value = "";
}
 */


function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}

  // Only number.
  setInputFilter(document.getElementById("telefonoContactoUsu"), function(value) {
    return /^-?\d*$/.test(value); });
  setInputFilter(document.getElementById("cedulaUsu"), function(value) {
    return /^-?\d*$/.test(value); });


// Only letter
  setInputFilter(document.getElementById("nombreCompletoUsu"), function(value) {
    return /^[[A-Za-z\s]*$/i.test(value); });
  setInputFilter(document.getElementById("personaContactoUsu"), function(value) {
    return /^[[A-Za-z\s]*$/i.test(value); });

