
var btnConsultar = document.getElementById('btn-consultar');
 function consulValid() {
  var nombreCompleto = document.getElementById('nombreCompleto').value;
  var epsAsociado = document.getElementById('epsAsociado').value;
  var TelefonoAcompañante = document.getElementById('TelefonoAcompañante').value;
  var nombreAcompañante = document.getElementById('nombreAcompañante').value;
  var direccionPaciente = document.getElementById('direccionPaciente').value;
  var antecedentesMedicos = document.getElementById('antecedentesMedicos').value;
  var antecedente = document.getElementById('antecedente').value;
  var motivosConsulta = document.getElementById('motivosConsulta').value;
  if(nombreCompleto == "" || epsAsociado == "" || TelefonoAcompañante == "" || nombreAcompañante == "" || direccionPaciente == "" || antecedentesMedicos == "" || motivosConsulta == "") {
    btnConsultar.disabled = true;
  }else {
    btnConsultar.disabled = false;
   
  } 
}

window.onload = (e) => { 
  consulValid();
  /* limpiar(); */
}

/* btnConsultar.addEventListener("click", function(){ 
  limpiar();
  window.location.pathname = "test_covid";
}); */

function limpiar() {
/*   nombreCompleto.value = "";
  epsAsociado.value = "";
  TelefonoAcompañante.value = "";
  nombreAcompañante.value = "";
  direccionPaciente.value = "";
  antecedentesMedicos.value = "";
  antecedente.value = "";
  motivosConsulta.value = ""; */
  btnConsultar.disabled = true;
}



  function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
      textbox.addEventListener(event, function() {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }
      });
    });
  }
  
  // Only number.
    setInputFilter(document.getElementById("TelefonoAcompañante"), function(value) {
      return /^-?\d*$/.test(value); });
    setInputFilter(document.getElementById("telefono"), function(value) {
      return /^-?\d*$/.test(value); });
    setInputFilter(document.getElementById("cedulaUsuario"), function(value) {
      return /^-?\d*$/.test(value); });

  // Only letter
    setInputFilter(document.getElementById("nombreCompleto"), function(value) {
      return /^[[A-Za-z\s]*$/i.test(value); });
    setInputFilter(document.getElementById("nombreAcompañante"), function(value) {
      return /^[[A-Za-z\s]*$/i.test(value); });
    setInputFilter(document.getElementById("persona_contacto"), function(value) {
      return /^[[A-Za-z\s]*$/i.test(value); });