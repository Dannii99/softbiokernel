function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}

// Only number.
setInputFilter(document.getElementById("anoExperienciaHosEdit"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("telefonoContactoHosEdit"), function(value) {
  return /^-?\d*$/.test(value); });

// Only letter
setInputFilter(document.getElementById("nombreCompletoHosEdit"), function(value) {
  return /^[[A-Za-z\s]*$/i.test(value); });