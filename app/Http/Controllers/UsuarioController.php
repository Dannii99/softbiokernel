<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hospital = DB::table('hospital')->get();

        $usuario = DB::table('usuario')->get();
        return view("crear_usuario", ["usuario" => $usuario, 'crear' => (!count($hospital) == 0)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $nombre = $request->input('nombreCompletoUsu');
        $cedula = $request->input('cedulaUsu');
        $direccion = $request->input('direccionUsu');
        $telefono = $request->input('telefonoContactoUsu');
        $persona_contacto = $request->input('personaContactoUsu');
      
        DB::table('usuario')->insert([
            [
            'nombre' => $nombre,
            'cedula' => $cedula,
            'direccion' =>$direccion,
            'telefono' => $telefono,
            'persona_contacto' => $persona_contacto
            ]
        ]);

        return redirect('/usuario');
    }

    public function createComplete(Request $request)
    {
        $nombre = $request->input('nombreCompleto');
        $cedula = $request->input('cedulaUsuario');
        $eps_asociado = $request->input('epsAsociado');
        $telefono = $request->input('telefono');
        $telefono_acompañante = $request->input('TelefonoAcompañante');
        $persona_contancto = $request->input('personaContacto');
        $nombre_acompañante = $request->input('nombreAcompañante');
        $direccion = $request->input('direccionPaciente');
        $antecedentes_Medicos = $request->input('antecedentesMedicos');
        $antecedentes = $request->input('antecedente');
        $motivos_consulta = $request->input('motivosConsulta');
      
        DB::table('usuario')->insert([
            [
            'nombre' => $nombre,
            'cedula' => $cedula,
            'eps' => $eps_asociado,
            'direccion' =>$direccion,
            'telefono' => $telefono,
            'telefono_acompañante' => $telefono_acompañante,
            'persona_contacto' => $persona_contancto,
            'nombre_acompañante' => $nombre_acompañante,
            'antecedente_medico' => $antecedentes_Medicos,
            'registrar_antecedentes' => $antecedentes,
            'motivos_consulta' => $motivos_consulta
            ]
        ]);

        return redirect("/test_covid/$cedula");
    }

    public function showConsult(Request $request)
    {
        $cedula = $request->input('cedulaUsu');

        $cliente = DB::table('usuario')->where('cedula', $cedula)->first();

        return view('consulta', ['cliente' => $cliente, 'mostrar' => (!empty($cliente)) ]);
    }

    public function showTest($documento)
    {
        $cliente = DB::table('usuario')->where('cedula', $documento)->first();

        if (empty($cliente)){
            return redirect('doctor');
        }

        return view('test_covid', ['cliente' => $cliente, 'mostrar' => (!empty($cliente)) ]);
    }

    public function submitTest($documento, Request $req)
    {
        
        $dolencias = [];

        for ($i=1; $i < 7; $i++) { 
            if (!empty($req->input($i)) && $req->input($i) == 'on'){
                array_push($dolencias, $req->input($i));
            }
        }

        $diagnostico = (count($dolencias) > 0) ? "Usted es posible para COVID-19" : "Usted esta sano";

        DB::table('usuario')->where('cedula', $documento)->update(
            [
                'diagnostico' => $diagnostico
            ]
        );

        return view('resultado', ['diagnostico' => $diagnostico ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show(Usuario $usuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */

    public function imprimirUsu($id)
    {
        $usuario = DB::table('usuario')->where('id', $id)->get();
        return view('editar_usuario', ['usuario' => $usuario[0]]);
    }
    

    public function edit($id, Request $req)
    {
        $usuario = DB::table('usuario')->where('id', $id)->update(
            [
                "nombre" => $req->nombreCompletoUsuEdit,
                "cedula" => $req->direccionUsuEdit,
                "direccion" => $req->direccionUsuEdit,
                "telefono" => $req->telefonoContactoUsuEdit,
                "persona_contacto" => $req->personaContactoUsuEdit,
            ]
        );
            return redirect('usuario');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {

        $usuario = DB::table('usuario')->where('id', $id)->update(
            [
            "nombre" => $req->nombreCompleto,
            'cedula' => $req->cedulaUsuario,
            'eps' => $req->epsAsociado,
            'direccion' => $req->direccion,
            'telefono' => $req->telefono,
            'telefono_acompañante' => $req->TelefonoAcompañante,
            'persona_contacto' => $req->personaContacto,
            'nombre_acompañante' => $req->nombreAcompañante,
            'antecedente_medico' => $req->antecedentesMedicos,
            'registrar_antecedentes' => $req->antecedente,
            'motivos_consulta' => $req->motivosConsulta,
            'direccion' => $req->direccionPaciente,
            ]
        );

        return redirect("/test_covid/$req->cedulaUsuario");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('usuario')->delete($id);

        return redirect('/usuario');
    }
}
