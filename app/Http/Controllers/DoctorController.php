<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       /*  $doctor = DB::table('doctor')->get();

        return view('crear_doctor', ['doctor'=>  $doctor]); */

        /* $doctor = Doctor::all(); */

        $hospital = DB::table('hospital')->get();
        $doctor = DB::table('doctor')->get();
        return view("crear_doctor", ["doctor" => $doctor, 'crear' => (!count($hospital) == 0)]);

    }

    public function index2()
    {
        $doctor = DB::table('doctor')->get();

        return view("doctor", ["doctor" => $doctor]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $nombre = $request->input('nombreCompletoHos');
        $direccion = $request->input('direccionHos');
        $telefono = $request->input('telefonoContactoHos');
        $tipo_sangre = $request->input('tipoSangreHos');
        $ano_experiencia = $request->input('anoExperienciaHos');
        $fecha_nacimiento = $request->input('fechaNacimientoHos');

        DB::table('doctor')->insert([
            [
            'nombre' => $nombre,
            'direccion' =>$direccion,
            'telefono' => $telefono,
            'tipo_sangre' => $tipo_sangre,
            'ano_experiencia' => $ano_experiencia,
            'fecha_nacimiento' => $fecha_nacimiento
            ]
        ]);

        return redirect('doctorC');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     * 
     * 
     */

    public function imprimirDoc($id)
    {
        $doctor = DB::table('doctor')->where('id', $id)->get();
        return view('editar_doctor', ['doctor' => $doctor[0]]);
    }
    
    public function edit($id, Request $req)
    {
        $doctor = DB::table('doctor')->where('id', $id)->update(
            [
                "nombre" => $req->nombreCompletoHosEdit,
                "direccion" => $req->direccionHosEdit,
                "telefono" => $req->telefonoContactoHosEdit,
                "tipo_sangre" => $req->tipoSangreHosEdit,
                "ano_experiencia" => $req->anoExperienciaHosEdit,
                "fecha_nacimiento" => $req->fechaNacimientoHosEdit
            ]
        );
            return redirect('doctorC');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctor $doctor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('doctor')->delete($id);

        return redirect('/doctorC');
    }
}
